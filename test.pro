#-------------------------------------------------
#
# Project created by QtCreator 2015-03-09T11:07:20
#
#-------------------------------------------------

QT       += core gui
QT       += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = test
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    datamanager.cpp

HEADERS  += mainwindow.h \
    datamanager.h

FORMS    += mainwindow.ui
