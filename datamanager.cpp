#include "datamanager.h"

DataManager * DataManager::singleton = NULL;

DataManager::DataManager(QObject *parent, const QString& path) : QObject(parent){

    this->db = QSqlDatabase::addDatabase("QSQLITE");
    this->db.setDatabaseName(path);
    qDebug() << "Db:" << this->db.open();
    this->execute("PRAGMA foreign_keys=ON;");
}

DataManager::~DataManager()
{
    delete DataManager::singleton;
    DataManager::singleton = NULL;

}

DataManager * DataManager::getInstance() {

    if(DataManager::DataManager::singleton == NULL) qDebug() << "Uninitialized";
    return DataManager::singleton;
}

DataManager * DataManager::createInstance(const QString &path) {

    if(DataManager::singleton != NULL) qDebug() << "Already Initialized";
    DataManager::singleton = new DataManager(NULL, path);
    if(DataManager::singleton->db.lastError().isValid()){

        QString s = "Could not initialize("+DataManager::singleton->db.lastError().text()+")";
//        int code = DataManager::singleton->db.lastError().number();

        delete DataManager::singleton;
        DataManager::singleton = NULL;

    }

    return DataManager::singleton;
}

void DataManager::execute(const QString& sql) {

    QSqlQuery query;

    if(!query.exec(sql)) qDebug() << "Could not execute" <<  sql << " Error:" << query.lastError() << " " << query.lastError();

}


int DataManager::getData(const QString &sql, QSqlQueryModel *resultSet) {

    resultSet->setQuery(sql);

    if(resultSet->lastError().isValid()) qDebug() << "Could not execute " << sql << " Error:" << resultSet->lastError() ;

    return resultSet->rowCount();
}


int DataManager::getSampleId() {
    QString sql;
    QSqlQueryModel qs;
    QSqlRecord r;

    sql = QString("select id, sid from report WHERE DATETIME(id, 'unixepoch', 'localtime') >= DATETIME('now', 'localtime', 'start of day') AND DATETIME(id, 'unixepoch', 'localtime') <= DATETIME('now', 'localtime')  order by id desc limit 1;");

    this->getData(sql, &qs);

    r = qs.record(0);

    if (r.value("id").isNull())
        return -1;

    return r.value("sid").toInt();
}

