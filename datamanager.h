#ifndef DATAMANAGER_H
#define DATAMANAGER_H

#include <QObject>
#include <QDebug>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQueryModel>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlError>

class DataManager : public QObject
{
    Q_OBJECT
public:
    static DataManager * createInstance(const QString& path) ;
    static DataManager * getInstance() ;
    ~DataManager();

signals:

public slots:
    void execute(const QString& sql);
    int getData(const QString& sql, QSqlQueryModel *resultSet) ;
    int getSampleId();
private:
    explicit DataManager(QObject *parent = 0, const QString& path="");
    static DataManager *singleton;
    QSqlDatabase db;

private slots:
};

#endif // DATAMANAGER_H
